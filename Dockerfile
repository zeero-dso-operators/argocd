FROM quay.io/operator-framework/ansible-operator:v1.0.0

USER root

RUN curl -LO https://get.helm.sh/helm-v3.5.3-linux-amd64.tar.gz && \ 
    tar vxf helm-v3.5.3-linux-amd64.tar.gz && \
    install linux-amd64/helm /usr/local/bin && \
    rm -r helm-v3.5.3-linux-amd64.tar.gz linux-amd64

USER ansible

COPY requirements.yml ${HOME}/requirements.yml
RUN ansible-galaxy collection install -r ${HOME}/requirements.yml \
 && chmod -R ug+rwx ${HOME}/.ansible

COPY watches.yaml ${HOME}/watches.yaml
COPY roles/ ${HOME}/roles/
COPY playbooks/ ${HOME}/playbooks/
